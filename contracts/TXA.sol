// SPDX-License-Identifier: GPL-3.0-or-later
/**
 *  Copyright (C) 2021 TXA Pte. Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
pragma solidity ^0.8.3;

import "@openzeppelin/contracts/token/ERC777/ERC777.sol";

contract TXA is ERC777 {
    address public manager;
    bool public transferFlag = false;
    mapping(address => bool) public distributors;
    mapping(address => bool) public restricted;
    /**
     * @dev `defaultOperators` may be an empty array.
     */
    constructor(
        address[] memory defaultOperators,
        address _manager,
        address founders,
        address operations,
        address advisors,
        address seed,
        address privateSale,
        address foundation,
        address reserve
    ) public ERC777("TXA", "TXA", defaultOperators) {
        manager = _manager;
        uint256 wholeTokenMul = 10 ** uint256(decimals());
        distributors[msg.sender] = true;
        distributors[manager] = true;
        distributors[founders] = true;
        distributors[operations] = true;
        distributors[advisors] = true;
        distributors[seed] = true;
        distributors[privateSale] = true;
        distributors[foundation] = true;
        distributors[reserve] = true;
        _mint(msg.sender, wholeTokenMul * 50000000, "", "");
        send(founders, wholeTokenMul * 8000000, "");
        send(operations, wholeTokenMul * 7000000, "");
        send(advisors, wholeTokenMul * 5000000, "");
        send(seed, wholeTokenMul * 1250000, "");
        send(privateSale, wholeTokenMul * 4375000, "");
        send(foundation, wholeTokenMul * 10000000, "");
        transfer(reserve, wholeTokenMul * 14375000);
        distributors[msg.sender] = false;
        require(balanceOf(msg.sender) == 0, "LEFTOVER_TOKENS");
    }

    modifier onlyManager {
        require(msg.sender == manager, "UNAUTHORIZED");
        _;
    }

    /**
     * Called by manager to allow non-distribution contracts to perform transfers.
     */
    function allowTransfers() external onlyManager {
        require(!transferFlag, "ALREADY_ACTIVATED");
        transferFlag = true;
    }

    /**
     * Called by manager to restrict addresses from receiving tokens.
     */
    function restrictTransfers(address[] calldata recipients) external onlyManager {
        for(uint i = 0; i < recipients.length; i++) {
            restricted[recipients[i]] = true;
        }
    }

    /**
     * Called before every transfer to check it should be allowed.
     * If manager has yet to call `allowTransfers`, then only distribution addresses are allowed to transfer.
     * Forbids transferring to an address that manager has set as restricted.
     */
    function _beforeTokenTransfer(address operator, address from, address to, uint256 amount) internal override { 
        if(!transferFlag) {
            require(from == address(0) || distributors[from], "NOT_DISTRIBUTOR");
        }
        require(!restricted[to], "FORBIDDEN_RECIPIENT");
    }
}
