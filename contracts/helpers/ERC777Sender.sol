// SPDX-License-Identifier: GPL-3.0-or-later
/**
 *  Copyright (C) 2021 TXA Pte. Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
pragma solidity ^0.8.3;

import "./ERC777Recipient.sol";
import "@openzeppelin/contracts/token/ERC777/IERC777Sender.sol";
import "@openzeppelin/contracts/token/ERC777/IERC777.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * Helper contract that can send and recieve ERC777 tokens
 */
contract ERC777Sender is ERC777Recipient, Ownable {
    constructor() {

    }

    function sendToken(address token, address recipient, uint256 amount, bytes calldata data) external onlyOwner {
        IERC777(token).send(recipient, amount, data);
    }

    function transferToken(address token, address recipient, uint256 amount) external onlyOwner {
        IERC20(token).transfer(recipient, amount);
    }

    function transferTokenFrom(address token, address sender, address recipient, uint256 amount) external onlyOwner {
        IERC20(token).transferFrom(sender, recipient, amount);
    }

    function operatorSendToken(address token, address sender, address recipient, uint256 amount, bytes calldata data, bytes calldata operatorData) external onlyOwner {
        IERC777(token).operatorSend(sender, recipient, amount, data, operatorData);
    }
}