/**
 *  Copyright (C) 2021 TXA Pte. Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import "@nomiclabs/hardhat-waffle";
import "@nomiclabs/hardhat-ethers";
import "hardhat-deploy";
import "hardhat-deploy-ethers"
import '@typechain/hardhat';
import "solidity-coverage"
import "hardhat-tracer"

export default {
    solidity: {
        compilers: [
            {
                version: "0.7.6"
            },
            {
                version: "0.6.2"
            },
            {
                version: "0.8.3",
                settings: {
                    modelChecker: {
                        targets: "assert,constantCondition,popEmptyArray,balance",
                        engine: "all"
                    }
                }
            }
        ]
    },
    defaultNetwork: "hardhat",
    networks: {
        hardhat: {
            live: false,
            saveDeployments: true,
            accounts: {
              count: 100
            },
            allowUnlimitedContractSize: true,
        },
        test_eth: {
            live: true,
            saveDeployments: true,
            url: "http://localhost:8547",
            accounts: {
                mnemonic: "pupil describe wild around truth fiber grocery solar oval repair oxygen drift",
                path: "m/44'/60'/0'/0",
                initialIndex: 0,
                count: 30
            }
        }
    },
    namedAccounts: {
        deployer: 0,
        alice: 7,
        bob: 8,
        charlie: 9,
        founders: 10,
        operations: 11,
        advisors: 12,
        seed: 13,
        privateSale: 14,
        foundation: 15,
        reserve: 16,
        nonDistributor: 17
    }
};
