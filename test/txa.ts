/**
 *  Copyright (C) 2021 TXA Pte. Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { deployments, ethers, getNamedAccounts } from "hardhat";
import { ERC777Sender } from "../typechain/ERC777Sender";
import { TXA } from "../typechain/TXA";
import { getDeployed, deployTokenSenders, wholeTokensToPrecision } from "./utils";
import { expect } from "chai";

describe("TXA", async () => {
    let deployer: string
    let alice: string
    let bob: string

    let founders: ERC777Sender
    let operations: ERC777Sender
    let advisors: ERC777Sender
    let seed: ERC777Sender
    let privateSale: ERC777Sender
    let foundation: ERC777Sender
    let reserve: ERC777Sender

    before(async () => {
        const accounts = await getNamedAccounts()
        deployer = accounts.deployer
        alice = accounts.alice
        bob = accounts.bob
    })


    beforeEach(async () => {
        const tokenSenders = await deployTokenSenders()
        founders = tokenSenders.founders
        operations = tokenSenders.operations
        advisors = tokenSenders.advisors
        seed = tokenSenders.seed
        privateSale = tokenSenders.privateSale
        foundation = tokenSenders.foundation
        reserve = tokenSenders.reserve
    })


    describe("after deploying", async () => {
        beforeEach(async () => {
            await deployments.deploy("TXA", {
                from: deployer,
                args: [
                    [],
                    deployer,
                    founders.address,
                    operations.address,
                    advisors.address,
                    seed.address,
                    privateSale.address,
                    foundation.address,
                    reserve.address
                ]
            })
        })


        it("distributes tokens correctly on deployment", async () => {
            let token = await getDeployed("TXA") as TXA
            interface AddressBalance {
                address: string, balance: number | string
            }
            const expectBalance = async (addrBal: AddressBalance) =>
                expect(await token.balanceOf(addrBal.address)).to.be.eq(wholeTokensToPrecision(ethers.BigNumber.from(addrBal.balance)))

            const addrBal = (address: string, balance: number | string): AddressBalance => { return { address, balance } }
            const expectedBalances: AddressBalance[] = [
                addrBal(founders.address, 8000000),
                addrBal(operations.address, 7000000),
                addrBal(advisors.address, 5000000),
                addrBal(seed.address, 1250000),
                addrBal(privateSale.address, 4375000),
                addrBal(foundation.address, 10000000),
                addrBal(reserve.address, 14375000),
            ]
            for (let expected of expectedBalances) {
                await expectBalance(expected)
            }
        })

        it("forbids non-distribution addresses from transfering", async () => {
            let aliceSigner = ethers.provider.getSigner(alice)
            let token = await getDeployed("TXA") as TXA

            await founders.transferToken(token.address, alice, wholeTokensToPrecision(50000))

            await expect(
                token.connect(aliceSigner).transfer(bob, 10000)
            ).to.be.revertedWith("NOT_DISTRIBUTOR")

            await expect(
                token.connect(aliceSigner).send(bob, 10000, "0x")
            ).to.be.revertedWith("NOT_DISTRIBUTOR")
        })

        it("allows non-distribution addresses to transfer after manager calls allowTransfers", async () => {
            let aliceSigner = ethers.provider.getSigner(alice)
            let token = await getDeployed("TXA") as TXA

            await founders.transferToken(token.address, alice, wholeTokensToPrecision(50000))

            await token.allowTransfers()
            
            await token.connect(aliceSigner).transfer(bob, 10000)
            expect(await token.balanceOf(bob)).to.be.eq(10000)
            await token.connect(aliceSigner).send(bob, 10000, "0x")
            expect(await token.balanceOf(bob)).to.be.eq(20000)
        })

        it("forbids non-manager from calling allowTransfers", async () => {
            let aliceSigner = ethers.provider.getSigner(alice)
            let token = await getDeployed("TXA") as TXA
            await expect(
                token.connect(aliceSigner).allowTransfers()
            ).to.be.revertedWith("UNAUTHORIZED")
        })

        it("allows manager to restrict an address from recieving tokens", async () => {
            let aliceSigner = ethers.provider.getSigner(alice)
            let token = await getDeployed("TXA") as TXA

            await founders.transferToken(token.address, alice, wholeTokensToPrecision(50000))

            await token.allowTransfers()
            
            await token.restrictTransfers([bob])

            await expect(
                token.connect(aliceSigner).transfer(bob, 10000)
            ).to.be.revertedWith("FORBIDDEN_RECIPIENT")

            await expect(
                token.connect(aliceSigner).send(bob, 10000, "0x")
            ).to.be.revertedWith("FORBIDDEN_RECIPIENT")
        })

        it("forbids non-manager from calling restrictTransfers", async () => {
            let aliceSigner = ethers.provider.getSigner(alice)
            let token = await getDeployed("TXA") as TXA
            await expect(
                token.connect(aliceSigner).restrictTransfers([bob])
            ).to.be.revertedWith("UNAUTHORIZED")
        })

    })

})
