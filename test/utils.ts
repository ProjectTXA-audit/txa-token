/**
 *  Copyright (C) 2021 TXA Pte. Ltd.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 *  See the GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
import { ethers, deployments } from "hardhat";
import { BigNumber, Signer } from "ethers";
import { ERC777Sender } from "../typechain/ERC777Sender";

export const TOTAL_COINS = 50000000
export const secondsInDay = 24 * 60 * 60
export const daysToSeconds = (days: number) => days * secondsInDay

/**
 * @description Given amount of whole ERC777 tokens, returns the amount with precision.
 * Per EIP777, any token that implements this standard always has 18 decimals of precision.
 * @param amount Number of whole tokens
 */
 export const wholeTokensToPrecision = (amount: BigNumber | number): BigNumber => {
    if(typeof amount == "number") {
        amount = ethers.BigNumber.from(amount)
    }
    return amount.mul(ethers.BigNumber.from(10).pow(18))
}

export const increaseTimeBySeconds = async (seconds: number | BigNumber) => {
    if (typeof seconds !== "number") {
        seconds = seconds.toNumber()
    }
    await ethers.provider.send("evm_increaseTime", [seconds])
    await ethers.provider.send('evm_mine', [])
}

export const setNextBlockTimestamp = async (timestamp: number | BigNumber) => {
    if (typeof timestamp !== "number") {
        timestamp = timestamp.toNumber()
    }
    await ethers.provider.send("evm_setNextBlockTimestamp", [timestamp])
}

export const getDeployed = async (contractName: string, signer?: Signer) => {
    const contract = await deployments.get(contractName)
    return await ethers.getContractAt(contract.abi, contract.address, signer)
}

/**
 * Deploys an ERC777Sender contract for each named account defined in the hardhat configuration.
 */
export const deployTokenSenders = deployments.createFixture(async ({ deployments, getNamedAccounts, ethers }, options) => {
    await deployments.fixture()

    const deployTokenSender = async (address: string) => 
        await (await ethers.getContractFactory("ERC777Sender", await ethers.getSigner(address))).deploy() as ERC777Sender
    const namesWithAddresses = Object.entries(await getNamedAccounts())
    let tokenSenders: {[name: string]: ERC777Sender} = {}
    for(let nameWithAddress of namesWithAddresses) {
        tokenSenders = Object.assign(
            {[nameWithAddress[0]]: await deployTokenSender(nameWithAddress[1])},
            tokenSenders
        )
    }
    console.log("deployed token senders");
    
    return tokenSenders;
});
